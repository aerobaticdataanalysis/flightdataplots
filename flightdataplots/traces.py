import plotly.graph_objects as go

def rolling(rate):
    if rate>0.01:
        return 1
    return 0

def checkbox(x, y, z):
    return x > limit(y) or z > limit(y) or x < -limit(y)


def boxcolour(x, y, z):
    if checkbox(x, y, z):
        return 1
    if y > 200 or y < 150:
        return 0.75
    return 0.5


def trace_plot(plotdata, colour=None, cmax=0.02, cmin=0, cscale='Bluered'):
    return go.Scatter3d(
        x=plotdata.iloc[:,0], 
        y=plotdata.iloc[:,1], 
        z=plotdata.iloc[:,2],
        mode='lines',
        line=dict(
            color=plotdata.loc[:,colour] if colour else 'blue',
            cmax=cmax,
            cmin=cmin,
            colorscale=cscale,
            #colorbar=dict(title='roll rate'),
            width=5
        ),
    )
 
def standard_layout(plotdata):
    return go.Layout(
        width = 1000,
        height = 600,
        margin=dict(l=10, r=10, t=0, b=10),
        scene= dict(
            xaxis = dict(nticks=10, range=[min(plotdata.iloc[:,0]), max(plotdata.iloc[:,0])]),
            yaxis = dict(nticks=10, range=[min(plotdata.iloc[:,1]), max(plotdata.iloc[:,1])]),
            zaxis = dict(nticks=10,range=[min(plotdata.iloc[:,2]), max(plotdata.iloc[:,2])]),
            aspectmode='data'
        ),
        scene_camera=dict(
            up=dict(x=0, y=0, z=1),
            eye=dict(x=0, y=-2, z=-0.6)
        )
    )


def box_trace(frontbox, backbox):
    return go.Mesh3d(
        x=frontbox['x'] + backbox['x'], 
        y=frontbox['y'] + backbox['y'], 
        z=frontbox['z'] + backbox['z'],
        color='silver', 
        opacity=0.25,
        i=[0, 0, 1, 1, 0, 3, 6, 6, 0, 0, 3, 3],
        j=[1, 2, 6, 5, 3, 7, 4, 5, 5, 4, 2, 6],
        k=[2, 3, 2, 6, 4, 4, 7, 4, 1, 5, 6, 7],
    )

def line_trace(linedict):
    return go.Scatter3d(
        x=linedict['x'], 
        y=linedict['y'], 
        z=linedict['z'],
        mode='lines',
        line=dict(
            color='black',
            width=1
        ),
    )

def limit(depth):
    return depth *tan(radians(60))

def box_square(depth): 
    lbox = limit(depth)
    return {
        'x': [-lbox, lbox, lbox, -lbox],
        'y': [depth for i in range(0,4)],
        'z': [0, 0, lbox, lbox]
    } 
  
def addlastline(squaredict):
    outdict = {}
    for key, value in squaredict.items():
        outdict[key] = value + [value[0]]
    return outdict